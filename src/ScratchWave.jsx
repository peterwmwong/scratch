import React, { Component } from 'react'
import './ScratchWave.css'

class ScratchWave extends Component {
  componentDidMount() {
    this.drawWave()
  }

  componentDidUpdate(prevProps) {
    const { buffer, height } = this.props
    const sameBuffer = prevProps.buffer === buffer
    const sameHeight = prevProps.height === height
    if (sameBuffer && sameHeight) return
    this.drawWave()
  }

  drawWave() {
    const { buffer, height } = this.props

    const waveResolution = 20
    if (!buffer) return

    const width = 200
    const channel = buffer.getChannelData(0)
    const sampleStep = Math.floor(channel.length / (height * waveResolution))
    const canvasCtx = this.canvas.getContext('2d')

    canvasCtx.save()
    canvasCtx.strokeStyle= `rgba(255, 215, 0, ${.05 + 1/waveResolution})`
    canvasCtx.translate(width / 2, height)
    canvasCtx.scale(1, -1)
    for(let i = 0; i < height * waveResolution; i++) {
      const amp = channel[i * sampleStep];
      const y = i / waveResolution
      canvasCtx.beginPath()
      canvasCtx.moveTo(0, y)
      canvasCtx.lineTo(-1 * amp * width / 2, y)
      canvasCtx.moveTo(0, y)
      canvasCtx.lineTo(1 * amp * width / 2, y)
      canvasCtx.stroke()
    }
    canvasCtx.restore()
  }

  render() {
    const { height, onClick } = this.props
    return (
      <div className="ScratchWave" onClick={onClick}>
        <canvas className="ScratchWave-canvas" ref={c => { this.canvas = c }} width={200} height={height} />
      </div>
    )
  }
}

export default ScratchWave
