import ctx from './audio_context'

const analyser = ctx.createAnalyser()
analyser.fftSize = 2048
analyser.connect(ctx.destination)

export default analyser
