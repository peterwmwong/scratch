import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './Scratch.css';

class Scratch extends Component {

  constructor(props) {
    super(props);

    this.state = {
      start: 0,
      end: 1,
      loaded: false,
      time: .5
    }

    this.setupAudioContext();
  }

  setupAudioContext() {
    this.audioCtx = new (window.AudioContext || window.webkitAudioContext)() // define audio context
    const request = new XMLHttpRequest()
    request.open('GET', '/sounds/Ahhh.wav', true);
    request.responseType = 'arraybuffer';
    request.onload = () => {
      this.audioCtx.decodeAudioData(request.response, (buffer) => {
        this.scratchBuffer = buffer
        this.drawWaveform();
        this.setState({ loaded: true })
      }, console.error)
    }
    request.send()
  }

  drawWaveform = () => {
    const chan = this.scratchBuffer.getChannelData(0);
    const samples = chan.length;
    const canvasCtx = ReactDOM.findDOMNode(this.canvas).getContext('2d');
    canvasCtx.save();
    canvasCtx.strokeStyle = '#000';
    canvasCtx.translate(200 / 2, 0);
    canvasCtx.globalAlpha = 0.2;
    for (var i = 0; i < 200; i++) {
      const x = chan[Math.floor(i / 200 * samples)];
      canvasCtx.fillRect(0, 200 - i, x * 200, 1);
    }
    canvasCtx.restore();
  }

  startAnimation = () => {
    requestAnimationFrame(this.startAnimation)

    if (this.audioCtx) {
      console.log(this.audioCtx.currentTime);
    }

  }

  addPoint = (x, y) => {
    console.log(x, y);
  }

  handleClick = (e) => {
    const el = ReactDOM.findDOMNode(this.pad);
    const h = el.offsetHeight
    const w = el.offsetWidth
    const x = e.clientX - el.offsetLeft
    const y = e.clientY - el.offsetTop

    if (x / w < .5) {
      this.setState({ start: 1 - y / h });
    }
    else {
      this.setState({ end: 1 - y / h });
    }
  }

  reverseBuffer(buffer) {
    const numChannels = buffer.numberOfChannels
    const newBuffer = this.audioCtx.createBuffer(numChannels, buffer.length, buffer.sampleRate)

    for (let i = 0; i < numChannels; i++) {
      const clonedChannel = new Float32Array(buffer.getChannelData(i))
      Array.prototype.reverse.call(clonedChannel);
      newBuffer.getChannelData(i).set(clonedChannel);
    }

    return newBuffer;
  }

  handleClickPlay = (e) => {
    const { start, end, time } = this.state
    const slope = end - start / 1
    const reverse = slope < 0;
    const bufferDuration = this.scratchBuffer.duration
    const source = this.audioCtx.createBufferSource()
    const startOffset = bufferDuration * start;
    const startTime = reverse ? bufferDuration - startOffset : startOffset

    const rate = Math.abs(bufferDuration * end - bufferDuration * start) / time;
    source.buffer = reverse ? this.reverseBuffer(this.scratchBuffer) : this.scratchBuffer;
    source.connect(this.audioCtx.destination)
    source.playbackRate.value = rate;
    source.start(this.audioCtx.currentTime, startTime);
    source.stop(this.audioCtx.currentTime + time);
  }

  handleChangeTime = (e) => {
    this.setState({ time: +e.target.value })
  }

  render() {
    return (
      <div>
        <input type="number" value={this.state.time} onChange={this.handleChangeTime}/>
        <div className="Scratch" ref={(pad) => { this.pad = pad }} onMouseUp={this.handleClick}>
          <canvas className="Scratch-waveForm" ref={canvas => { this.canvas = canvas }}/>
          <div className="Scratch-point" style={{left: 0, bottom: `${this.state.start * 100}%`}}>
            {this.state.start}
          </div>
          <div className="Scratch-point" style={{left: '100%', bottom: `${this.state.end * 100}%`}}>
            {this.state.end}
          </div>
        </div>
        {this.state.loaded &&
          <button onClick={this.handleClickPlay}>Play</button>
        }
      </div>
    )
  }
}

export default Scratch;
