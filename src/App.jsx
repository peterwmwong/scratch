import React, { Component } from 'react'

import './App.css'
import scratchPlayer from './scratch_player'
import analyser from './analyser'

import Grid from './Grid'
import Timeline from './Timeline'
import CrossFader from './CrossFader'
import ScratchControls from './ScratchControls'

import PointModel from './models/Point'
import FaderMovementModel from './models/FaderMovement'
import AnalyserViz from './AnalyserViz'

import loadScratch from './load_scratch'

const MIN_GRID_UNIT_SIZE = 5
const MAX_GRID_UNIT_SIZE = 30

const SCRATCHES = ['/sounds/Ahhh.wav', '/sounds/Ahh yeah.wav']

class App extends Component {
  constructor(props) {
    super(props)

    const defaultGridUnitWidth = (MAX_GRID_UNIT_SIZE - MIN_GRID_UNIT_SIZE) / 2 + MIN_GRID_UNIT_SIZE

    this.state = {
      height: 200,
      points: [new PointModel({ unit: 0, start: 0 })],
      faderPoints: [new FaderMovementModel({ unit: 0, state: 1 })],
      gridUnitWidth: defaultGridUnitWidth,
      beats: 12,
      bpm: 90,
      crossFaderEnabled: true,
      scratch: 0
    }
  }

  loadScratch(index) {
    this.scratchBuffer = undefined
    this.setState({ bufferLoaded: false, scratch: index })
    loadScratch(SCRATCHES[index]).then(buffer => {
      this.scratchBuffer = buffer
      this.setState({ bufferLoaded: true })
    })
  }

  componentDidMount() {
    this.loadScratch(this.state.scratch)
    this.workspace.focus()
  }

  componentDidUpdate() {
    this.workspace.focus()
  }

  addFaderPoint(x, state) {
    const { gridUnitWidth, faderPoints } = this.state
    const faderPoint = new FaderMovementModel({ x, state, gridUnitWidth })
    const changes = FaderMovementModel.changePoints([...faderPoints, faderPoint])
    this.setState({ faderPoints: changes })
  }

  addPoint(x, y) {
    const { gridUnitWidth, height } = this.state
    this.setState({ points: [...this.state.points, new PointModel({ x, y, gridUnitWidth, height })].sort(PointModel.sort) })
  }

  handleRemovePoint(e, pointIndex) {
    e.preventDefault()
    e.stopPropagation()
    if (pointIndex === 0) return;
    const points = [...this.state.points]
    points.splice(pointIndex, 1)
    this.setState({ points })
  }

  dragPoint(e, dragData, pointIndex) {
    const { points, gridUnitWidth, height, beats } = this.state
    const point = points[pointIndex]
    const nextPoint = points[pointIndex + 1]
    const prevPoint = points[pointIndex - 1]

    const pointY = point.y(height)
    const deltaY = e.shiftKey ? Math.sign(dragData.deltaY) * 2: dragData.deltaY
    const y = pointY + deltaY
    point.setStart(y, height)

    if (pointIndex !== 0 && !e.shiftKey) {
      const min = prevPoint.x(gridUnitWidth) + gridUnitWidth
      const max = nextPoint ? nextPoint.x(gridUnitWidth) - gridUnitWidth : beats * 16 * gridUnitWidth
      const x = Math.max(Math.min(dragData.x, max), min)
      point.setUnit(x, gridUnitWidth)
    }

    this.setState({ points })
  }

  dragSegmentStart(e, dragData, segment) {
    const { height, points } = this.state;
    segment.points.forEach(point => {
      const pointY = point.y(height)
      const deltaY = e.shiftKey ? Math.sign(dragData.deltaY) * 2: dragData.deltaY
      const y = pointY + deltaY
      point.setStart(y, height)
    })
    this.setState({ points })
  }

  play() {
    const { bpm, height, gridUnitWidth, points, faderPoints, crossFaderEnabled } = this.state
    const scratchSegments = PointModel.segments(points, { height, gridUnitWidth, bpm })
    const faderSegments = crossFaderEnabled ? FaderMovementModel.segments(faderPoints, { gridUnitWidth, bpm }) : []
    scratchPlayer(this.scratchBuffer, scratchSegments, faderSegments, analyser)
  }

  handleChangeBpm(e) {
    this.setState({ bpm: +e.target.value })
  }

  handleResizeGrid(delta) {
    const height = this.state.height + delta
    if (height < 100 || height > 500) return
    this.setState({ height: height })
  }

  handleScaleSegment(segments, segmentIndex, dragData) {
    const { gridUnitWidth } = this.state
    const segmentsToScale = segments.slice(0, segmentIndex + 1)
    PointModel.scaleSegments(segmentsToScale, dragData.deltaX, gridUnitWidth)
    this.setState({ points: this.state.points })
  }

  handleKeyDownWorkspace(e) {
    if (e.key === ' ') {
      e.preventDefault()
      this.play()
    }
    if (e.key === 'c') {
      e.preventDefault()
      this.setState({ crossFaderEnabled: !this.state.crossFaderEnabled })
    }
    if (e.key === 's') {
      e.preventDefault()
      const { gridUnitWidth } = this.state;
      this.setState({ gridUnitWidth: Math.max(gridUnitWidth - 2, MIN_GRID_UNIT_SIZE) })
    }
    if (e.key === 'w') {
      e.preventDefault()
      const { gridUnitWidth } = this.state;
      this.setState({ gridUnitWidth: Math.min(gridUnitWidth + 2, MAX_GRID_UNIT_SIZE) })
    }
  }

  handleToggleCrossFader(e) {
    this.setState({ crossFaderEnabled: e.target.checked })
  }

  handleToggleScratch(e) {
    const { scratch } = this.state
    const nextScratch = scratch + 1 >= SCRATCHES.length ? 0 : scratch + 1
    this.loadScratch(nextScratch)
  }

  render() {
    const { bpm, height, beats, gridUnitWidth, points, faderPoints, crossFaderEnabled } = this.state
    const scratchSegments = PointModel.segments(points, { height, gridUnitWidth, bpm })
    const faderSegments = FaderMovementModel.segments(faderPoints, { gridUnitWidth, bpm })

    return (
      <div>
        <div className="Controls u-flex u-flexAlignItemsCenter">
          <div className="Controls-space">
            <input
              className="Controls-bpm"
              type="number"
              value={bpm}
              onChange={(e) =>
              this.handleChangeBpm(e)}
            />
          </div>
          <div className="Controls-space">
            <input
              type="range"
              min={MIN_GRID_UNIT_SIZE}
              max={MAX_GRID_UNIT_SIZE}
              value={gridUnitWidth}
              onChange={(e) => this.setState({ gridUnitWidth: +e.target.value })}
            />
          </div>
          <div className="Controls-space">
            <button onClick={() => this.play()}>Play</button>
          </div>
          <div className="Controls-space">
            <AnalyserViz analyser={analyser} />
          </div>
        </div>
        <div
          className="Workspace"
          ref={w => { this.workspace = w }}
          tabIndex="0"
          onKeyDown={this.handleKeyDownWorkspace.bind(this)}
        >
          <ScratchControls
            height={height}
            buffer={this.scratchBuffer}
            crossFaderEnabled={crossFaderEnabled}
            onToggleCrossFader={this.handleToggleCrossFader.bind(this)}
            onToggleScratch={this.handleToggleScratch.bind(this)  }
          />
          <div className="App">
            <Timeline beats={beats} gridUnitWidth={gridUnitWidth} />
            <Grid
              height={height}
              gridUnitWidth={gridUnitWidth}
              points={points}
              beats={beats}
              segments={scratchSegments}
              onAddPoint={this.addPoint.bind(this)}
              onDragPoint={this.dragPoint.bind(this)}
              onRemovePoint={this.handleRemovePoint.bind(this)}
              onDragSegmentStart={this.dragSegmentStart.bind(this)}
              onResizeGrid={this.handleResizeGrid.bind(this)}
              onScaleSegment={this.handleScaleSegment.bind(this, scratchSegments)}
            />
            <CrossFader
              segments={faderSegments}
              gridUnitWidth={gridUnitWidth}
              onAddPoint={this.addFaderPoint.bind(this)}
              enabled={crossFaderEnabled}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default App
