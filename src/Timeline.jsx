import React from 'react';
import cx from 'classnames';
import './Timeline.css';

export default ({ beats, gridUnitWidth }) => (
  <div className="Timeline">
    {Array(16 * beats).fill().map((note, i) => {
      const quarterNote = Math.floor(i / 16) + 1 % 4
      const sixteenthNote = i % 16 / 4 + 1
      const isQuarterNote = i % 16 === 0
      const isSixteenthNote = i % 4 === 0
      const className = cx('Timeline-tick', {
        'Timeline-tick--primary': isQuarterNote,
        'Timeline-tick--secondary': isSixteenthNote && !isQuarterNote
      })
      return (
        <div
          key={i}
          className={className}
          style={{ width: gridUnitWidth }}
        >
          <div className="Timeline-tickLabel">
            {isQuarterNote && gridUnitWidth <= 10 && `${quarterNote}`}
            {isSixteenthNote && gridUnitWidth > 10 && `${quarterNote}.${sixteenthNote}`}
          </div>
        </div>
      )
    })}
  </div>
)
