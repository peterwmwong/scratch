import React, { Component } from 'react';
import { DraggableCore } from 'react-draggable'
import cx from 'classnames';

class Segment extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  startDragStart() {
    this.setState({ dragStart: true })
  }
  stopDragStart() {
    this.setState({ dragStart: false })
  }

  render() {
    const { segment, height, onDragStart, onScaleSegment } = this.props;
    return (
      <div
        className="Grid-segment"
        style={{
          width: segment.width,
        }}
      >
        <svg width={segment.width} height={height}>
          <line
            className="Grid-segmentLine"
            x1={0} x2={segment.width}
            y1={segment.y1} y2={segment.y2}
          />
          <DraggableCore
            onStart={this.startDragStart.bind(this)}
            onDrag={onDragStart}
            onStop={this.stopDragStart.bind(this)}
          >
            <line
              className={cx('Grid-segmentLineDragHandle', { 'is-dragging': this.state.dragStart })}
              x1={0} x2={segment.width}
              y1={segment.y1} y2={segment.y2}
            />
          </DraggableCore>
        </svg>
        <DraggableCore
          onDrag={(e, d) => onScaleSegment(d)}
        >
          <div className="Grid-segmentGridLine" />
        </DraggableCore>
      </div>
    )
  }
}

export default Segment
