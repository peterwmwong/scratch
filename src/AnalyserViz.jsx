import React, { Component } from 'react'

class AnalyserViz extends Component {

  constructor(props) {
    super(props)
    this.draw = this.draw.bind(this)
  }

  componentDidMount() {
    this.bufferLength = this.props.analyser.frequencyBinCount
    this.dataArray = new Uint8Array(this.bufferLength)
    this.ctx = this.canvas.getContext('2d')
    this.draw()
  }

  draw() {
    this.frame = window.requestAnimationFrame(this.draw)
    const { height, width } = this.canvas
    const { analyser } = this.props
    analyser.getByteTimeDomainData(this.dataArray)

    this.ctx.clearRect(0, 0, width, height)

    this.ctx.lineWidth = 2;
    this.ctx.strokeStyle = 'gold';

    this.ctx.beginPath();

    const sliceWidth = width * 1.0 / this.bufferLength;
    let x = 0;

    for (let i = 0; i < this.bufferLength; i++) {

      const v = this.dataArray[i] / 128.0;
      const y = v * height / 2;

      if (i === 0) {
        this.ctx.moveTo(x, y);
      }
      else {
        this.ctx.lineTo(x, y);
      }

      x += sliceWidth;
    }

    this.ctx.lineTo(width, height / 2);
    this.ctx.stroke();

  }


  render() {
    return (
      <div>
        <canvas ref={c => { this.canvas = c }} width="500" height="60" />
      </div>
    )
  }
}

export default AnalyserViz
