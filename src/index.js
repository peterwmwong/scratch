import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'normalize.css/normalize.css';
import './index.css';
import 'suitcss-utils-flex';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
