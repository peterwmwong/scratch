import React, { Component } from 'react'
import './ScratchControls.css'

import ScratchWave from './ScratchWave'

class ScratchControls extends Component {
  render() {
    const { height, buffer, crossFaderEnabled, onToggleCrossFader, onToggleScratch } = this.props
    return (
      <div className="ScratchControls" style={{ height: height }}>
        {buffer &&
          <ScratchWave buffer={buffer} height={height} onClick={onToggleScratch} />
        }
        <div style={{ margin: 8, textAlign: 'right' }}>
          <input type="checkbox" checked={crossFaderEnabled} onChange={onToggleCrossFader} />
        </div>
      </div>
    )
  }
}

export default ScratchControls
