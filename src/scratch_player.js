import ctx from './audio_context';

function reverseBuffer(buffer) {
  const numChannels = buffer.numberOfChannels
  const newBuffer = ctx.createBuffer(numChannels, buffer.length, buffer.sampleRate)

  for (let i = 0; i < numChannels; i++) {
    const clonedChannel = new Float32Array(buffer.getChannelData(i))
    Array.prototype.reverse.call(clonedChannel);
    newBuffer.getChannelData(i).set(clonedChannel);
  }

  return newBuffer;
}

function playSegment(segment, bufferF, bufferR, gainNode) {
  const source = ctx.createBufferSource()

  const reverse = segment.start > segment.end
  const start = reverse ? 1 - segment.start : segment.start
  const end = reverse ? 1 - segment.end : segment.end
  source.buffer = reverse ? bufferR : bufferF

  const bufferDuration = source.buffer.duration
  const sampleDuration = bufferDuration * end - bufferDuration * start

  const startTime = ctx.currentTime + segment.startOffset
  const startOffset = bufferDuration * start
  const stopTime = startTime + segment.duration
  const rate = sampleDuration / segment.duration

  source.connect(gainNode)
  source.playbackRate.value = rate
  source.start(startTime, startOffset)
  source.stop(stopTime)
}

function moveFader(segment, gainNode) {
  const { state, time } = segment
  const changeTime = ctx.currentTime + time
  gainNode.gain.setValueAtTime(state, changeTime)
}

export default function(buffer, scratchSegments, faderSegments, output = ctx.destination) {
  const bufferF = buffer
  const bufferR = reverseBuffer(buffer)

  const gainNode = ctx.createGain()
  gainNode.connect(output)

  scratchSegments.forEach(segment => {
    playSegment(segment, bufferF, bufferR, gainNode)
  });

  faderSegments.forEach(segment => {
    moveFader(segment, gainNode)
  })
}
