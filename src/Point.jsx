import React from 'react';
import { DraggableCore } from 'react-draggable'

export default ({
  className,
  top,
  left,
  onDrag,
  onRemove,
  point,
  children
}) => (
  <DraggableCore
    onDrag={onDrag}
  >
    <div
      className={className}
      style={{ top: top, left: left }}
      onDoubleClick={onRemove}
    >
      {children}
    </div>
  </DraggableCore>
)
