class Point {
  constructor({ x, y, unit, start, gridUnitWidth, height }) {
    if (unit != null && start != null) {
      this.unit = unit
      this.start = start
    }
    else {
      this.setUnit(x, gridUnitWidth)
      this.setStart(y, height)
    }
  }

  x(gridUnitTime) {
    return this.unit * gridUnitTime
  }

  y(height) {
    return height - this.start * height
  }

  setUnit(x, gridUnitTime) {
    if(x < 0) return
    this.unit = Math.round(x / gridUnitTime)
  }

  setStart(y, height) {
    if (y < 0 || y > height) return
    this.start = (height - y) / height
  }

  toString() {
    return `Point: ${this.unit} ${this.start}`
  }

  static sort(a, b) {
    return a.unit - b.unit
  }

  static segments(points, { height, gridUnitWidth, bpm }) {
    const segments = []
    const gridUnitTime = 15 / bpm

    points.forEach((point, i) => {
      const nextPoint = points[i + 1]
      if (!nextPoint) return

      const start = point.start
      const end = nextPoint.start
      const width = (nextPoint.unit - point.unit) * gridUnitWidth
      const duration = width / gridUnitWidth / 4 * gridUnitTime
      const previousSegment = segments[segments.length - 1]
      const startOffset = previousSegment ? previousSegment.startOffset + previousSegment.duration : 0

      segments.push({
        startOffset,
        start,
        end,
        width,
        duration,
        y1: point.y(height),
        y2: nextPoint.y(height),
        points: [point, nextPoint]
      })
    })

    return segments
  }

  static scaleSegments(segments, deltaX, gridUnitWidth) {
    const totalWidth = segments.reduce((width, segment) => width + segment.width, 0)
    const resized = totalWidth + deltaX
    const scale = resized / totalWidth

    segments.forEach(segment => {
      const point = segment.points[1]
      point.unit *= scale
    })
  }
}

export default Point
