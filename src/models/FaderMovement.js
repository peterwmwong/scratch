class FaderMovement {
  constructor({ unit, state, x, gridUnitWidth }) {
    if (unit != null) this.unit = unit
    else this.setUnit(x, gridUnitWidth)
    this.state = state
  }

  x(gridUnitWidth) {
    return this.unit * gridUnitWidth
  }

  setUnit(x, gridUnitWidth) {
    this.unit = x / gridUnitWidth
  }

  static changePoints(points) {
    const sortedPoints = points.sort((a, b) => a.unit - b.unit)
    const changes = [sortedPoints[0]]

    sortedPoints.forEach((point, i) => {
      if (i === 0) return
      const prevPoint = sortedPoints[i - 1]
      const samePosition = prevPoint.unit === point.unit

      if (samePosition) {
        prevPoint.state = point.state
      }

      if (point.state !== prevPoint.state) {
        changes.push(point)
      }

    })
    return changes
  }

  static segments(points, { gridUnitWidth, bpm }) {
    const segments = []

    const gridUnitTime = 15 / bpm

    points.forEach((point, i) => {
      const nextPoint = points[i + 1]
      const isFirst = i === 0
      const isLast = !nextPoint
      const unit = isFirst ? 0 : point.unit
      const left = gridUnitWidth * unit
      const width = isLast ? undefined : nextPoint.x(gridUnitWidth) - left
      const right = isLast ? 0 : undefined
      const state = point.state
      const time = unit / 4 * gridUnitTime
      segments.push({ left, right, width, state, unit, isLast, isFirst, time })
    })

    return segments
  }
}

export default FaderMovement
