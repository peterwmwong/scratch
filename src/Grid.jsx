import React, { Component } from 'react';
import { findDOMNode } from 'react-dom'
import { DraggableCore } from 'react-draggable'

import Point from './Point'
import Segment from './Segment'

import './Grid.css'

const StartGridLine = ({ top }) => (
  <div className="Grid-startGridLine" style={{ top }} />
)


class Grid extends Component {

  addPoint(e) {
    const el = findDOMNode(this.grid)
    const rect = el.getBoundingClientRect()
    const x = e.clientX - rect.left
    const y = e.clientY - rect.top
    this.props.onAddPoint(x, y)
  }

  handleDragResize(e, d) {
    this.props.onResizeGrid(d.deltaY)
  }

  render() {
    const {
      height,
      beats,
      gridUnitWidth,
      points,
      segments,
      onDragPoint,
      onRemovePoint,
      onDragSegmentStart,
      onScaleSegment
    } = this.props

    return (
      <div>
        <div
          className="Grid"
          style={{ height: height, width: 16 * beats * gridUnitWidth}}
          onDoubleClick={this.addPoint.bind(this)}
          ref={grid => { this.grid = grid }}
        >
          {points.map((point, i) => (
            <div key={i}>
              <Point
                className="Grid-point"
                point={point}
                left={point.x(gridUnitWidth)}
                top={point.y(height)}
                onDrag={(e, d) => onDragPoint(e, d, i)}
                onRemove={(e) => onRemovePoint(e, i)}
              />
              <StartGridLine top={point.y(height)} />
            </div>
          ))}
          {segments.map((segment, i) => (
            <Segment
              key={i}
              segment={segment}
              height={height}
              onDragStart={(e, d) => onDragSegmentStart(e, d, segment)}
              onScaleSegment={(d) => onScaleSegment(i, d)}
            />
          ))}
        </div>
        <DraggableCore onDrag={this.handleDragResize.bind(this)}>
          <div className="Grid-resizeHandle">
          </div>
        </DraggableCore>
      </div>
    )
  }
}

export default Grid
