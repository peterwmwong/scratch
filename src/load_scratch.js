import ctx from './audio_context';
export default function(path) {
  const request = new XMLHttpRequest()
  request.open('GET', path, true);
  request.responseType = 'arraybuffer';

  return new Promise((accept, reject) => {
    request.onload = () => {
      ctx.decodeAudioData(request.response, (buffer) => {
        accept(buffer)
      }, reject)
    }
    request.send()
  })
}
