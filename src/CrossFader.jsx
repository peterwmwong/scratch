import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import { DraggableCore } from 'react-draggable'
import cx from 'classnames'
import './CrossFader.css'

class CrossFader extends Component {
  constructor(props) {
    super(props)
    this.state = { height: 50 }
  }

  addCrossFaderMovement(e, d) {
    const { height } = this.state
    const el = findDOMNode(this)
    const rect = el.getBoundingClientRect()
    const x = e.clientX - rect.left
    const y = e.clientY - rect.top

    const state = y / height > .5 ? 0 : 1

    this.props.onAddPoint(x, state);
  }

  render() {
    const { segments, enabled } = this.props
    const { height } = this.state

    const className= cx('CrossFader', { 'is-disabled': !enabled })
    return (
      <DraggableCore
        onDrag={this.addCrossFaderMovement.bind(this)}
      >
        <div className={className} style={{ height }}>
          {segments.map((segment, i) => (
            <div
              key={i}
              className={cx('CrossFader-segment', {
                'is-off': !segment.state,
                'is-first': segment.isFirst,
                'is-last': segment.isLast
              })}
              style={{
                left: segment.left,
                width: segment.width,
                right: segment.right,
              }}
            />
          ))}
        </div>
      </DraggableCore>
    )
  }
}

export default CrossFader
